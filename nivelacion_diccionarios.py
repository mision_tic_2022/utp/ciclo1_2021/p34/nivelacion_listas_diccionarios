
"""
REPOSITORIO:
https://gitlab.com/mision_tic_2022/utp/ciclo1_2021/p34
"""
"""
Desarrollar un software para un supermercado que solicite al usuario
el nombre y el total de compra realizado por cada cliente,
esta información almacenarla en una lista de diccionarios.
Debe imprimir en pantalla el promedio de ventas realizadas
"""
#Estructura de una lista y diccionarios
"""
mi_diccionario = {"nombre": "juan", "edad": 20}
mi_diccionario2 = {"nombre": "ana", "edad": 20}
lista: list = [mi_diccionario, mi_diccionario2]
"""


def calcular_promedio(lista_compras: list)->float:
    #Variable que almacena la sumatoria de las compras
    total_compra: float = 0
    #Ciclo para iterar la lista
    for factura in lista_compras:
        #Accede al valor de la clave 'total_compra' de cada diccionario
        #y lo suma en la variable 'total_compra'
        total_compra += factura["total_compra"]
    #Calculamos el promedio con el total de las compras y
    #el tamaño de la lista de compras (cantidad de compras)
    promedio = total_compra/len(lista_compras)
    #Retornamos el promedio
    return promedio
    

def pedir_datos():
    #Se crea una lista de compras vacia (esta lista contendrá N diccionarios)
    lista_compras: list = []
    opc = 'S'
    #hola.upper() -> HOLA, s.upper() -> S
    while opc.upper() == 'S':
        #Solicitamos el nombre del cliente
        nombre_cliente: str = input("Por favor ingrese el nombre del cliente: ")
        #Solicitamos el total de la compra del cliente
        total_compra: float = float( input("Por favor ingrese el total de la compra del cliente: ") )
        #Creamos un diccionario con los datos de la compra
        datos_cliente: dict = {
            "cliente": nombre_cliente,
            "total_compra": total_compra
        }
        #Añadimos el diccionario a la lista de compras
        lista_compras.append(datos_cliente)
        opc = input("¿Desea continuar? S(si) - N(no): ")
    #Obtiene el promedio de compras
    promedio = calcular_promedio(lista_compras)
    #Muestra el promedio redondeado a dos decimales
    print("El promedio de compras es: ", round(promedio, 2) )

#pedir_datos()

"""
REPASO PASO DE DICCIONARIOS A FUNCIONES
"""
def datos_cliente(cliente: str, total_compra: float):
    print("Cliente: ", cliente)
    print("Total compra: ", total_compra)
    mi_diccionario = {
        "cliente": cliente,
        "total_compra": total_compra
    }
    datos_cliente_mejorado(mi_diccionario)

def datos_cliente_mejorado(compra_cliente: dict):
    print("Cliente: ", compra_cliente["cliente"])
    print("Total compra: ", compra_cliente["total_compra"])

cliente: dict = {
    "total_compra": 45.5,
    "cliente": "Andres"
}
#Desempaquetar un diccionario
datos_cliente(**cliente)
#datos_cliente_mejorado(cliente)

"""
#REPASO LAMBDA
def suma(num1: int, num2: int):
    return num1 + num2

func_suma = suma

lambda_suma = lambda num1, num2 : num1+num2

print( lambda_suma(5,4) )
"""